const mongoose = require("mongoose");

const stockSchema = new mongoose.Schema({

	stockItem: {
		type: String,
		required: [true, "Stock item name is required"]
	},
	stockDescription: {
		type: String,
		required: [true, "Stock Description is required"]
	},
	stockQty: {
		type: Number,
		required: [true, "How many items are in stock?"]
	},
	stockPrice: {
		type: Number,
		required: [true, "How much did you buy this item?"]
	},
	sellingPrice: {
		type: Number,
		required: [true, "Selling price is required. Please refer to the stock price for your reference"]
	},

	isActive: {
		type: Boolean,
		default: true
	}
})

module.exports = mongoose.model("Stocks", stockSchema);