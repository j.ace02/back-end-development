const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "First name is required"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required"]
	},

	address: {

		type: String,
		required: [true, "Complete Address is required"]

	},

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required for order confirmation and delivery"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	isActive: {
		type: Boolean,
		default: false
	}
})

module.exports = mongoose.model("User", userSchema);