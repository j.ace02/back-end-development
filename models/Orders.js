const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	orderName: {
		type:String,
		required: [true, "Order Item is required"]
	},

	orderQty: {
		type: Number,
		required: [true, "Order Quantity is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: Date.now
	},

	userOrder: [
		
		{

			email: {
				type: String,
				required: [true, "Email Address is required"]
			},

			orderList: {
				type: String,
				required: true
			}


		}

	]
})

module.exports = mongoose.model("Orders", orderSchema);