const express = require("express");
const router = express.Router();

const stockControllers = require("./../controllers/stockControllers");

const auth = require("./../auth");

//add new stock
router.post ("/addstock", auth.verify, (req,res) => {
	let stockData = {
		stockItem: req.body.stockItem,
		stockDescription: req.body.stockDescription,
		stockQty: req.body.stockQty,
		stockPrice: req.body.stockPrice,
		sellingPrice: req.body.sellingPrice
	}
	stockControllers.addStock(stockData).then( result => res.send(result))
})

//retrieve stock

router.get ("/view-all", (req,res) => {
	stockControllers.getStocks(req.body).then( result => res.send(result))
})

//replenish stock
router.put ("/:stockId/replenish", auth.verify, (req,res) => {
	stockControllers.replenish(req.params.stockId,req.body).then( result => res.send(result))
})

//remove stock
router.delete ("/:stockId/delete-stock", auth.verify,(req,res) => {
	stockControllers.deleteStocks (req.params.stockId,req.body).then( result => res.send(result))
})

//retrieving only active stock
router.get("/:stockId/active-stocks", auth.verify, (req,res) => {
	stockControllers.getActiveStocks(req.params.stockId,req.body)
	.then(result => res.send(result))
})

//archive stocks
router.put("/archive-stocks", auth.verify, (req,res) => {
	stockControllers.archiveStocks(req.body.stockItem).then( result => res.send(result))
})


//unarchive stocks
router.put("/unarchive-stocks", auth.verify, (req,res) => {
	stockControllers.unarchiveStocks(req.body.stockItem).then ( result => res.send(result))
})



module.exports = router;