const express = require("express");
const router = express.Router();

const userControllers = require("./../controllers/userControllers");

const auth = require("./../auth");

//check if email exists 
router.post("/email-exists", (req, res) => {

	userControllers.checkEmail(req.body).then( result => res.send(result))
})

//register a user
// http://localhost:4000/api/users
router.post("/register", (req, res) => {

	console.log (req.body)

	userControllers.register(req.body).then( result => res.send(result))
})

//set user as admin
router.put("/:userId/setAsAdmin", auth.verify,(req,res) => {

	userControllers.setAsAdmin(req.params.userId,req.body).then ( result => res.send(result))
})

router.get("/", (req, res) => {

	userControllers.getAllUsers().then( result => res.send(result))
})

//login user
router.post("/login",(req, res) => {

	userControllers.login(req.body).then(result => res.send(result))
})

//retrieve user information
router.get("/details", auth.verify,(req, res) => {
	
	let userData = auth.decode(req.headers.authorization)
	// console.log(userData)

	userControllers.getProfile(userData).then(result => res.send(result))
})

//inactive admin - when an admin terminates the employee or the employee is on leave
router.put("/:userId/setadmin-inactive", auth.verify, (req,res) => {


})

//re-active admin - when the employee is re-hired or back from leave
router.put("/:userId/setadmin-active", auth.verify, (req,res) => {



})



module.exports = router;