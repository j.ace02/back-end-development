const express = require("express");
const router = express.Router();

const orderControllers = require("./../controllers/orderControllers");

const auth = require("./../auth");

//create order

router.post ("/create-order", auth.verify, (req,res) => {
	orderControllers.createOrder(req.body).then( result => res.send(result))
})

//get all orders

router.get ("/products", (req,res) => {
	orderControllers.allActiveOrders(req.body).then (result => res.send(result))
})

//get single order

router.get ("/products/:productId", auth.verify, (req,res) => {
	orderControllers.singleOrder (req.params.productId).then (result => res.send(result))
})

//delete order

router.delete ("/delete-order", auth.verify, (req,res) => {
	orderControllers.deleteOrders (req.body).then( result => res.send(result))
})

//edit order
router.put ("/:productId/edit-order", auth.verify, (req,res) => {
	orderControllers.editOrders(req.params.productId, req.body).then( result => res.send(result))
})

module.exports = router;