const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 4000;
const cors = require("cors");

/*import routes module*/
const userRoutes = require("./routes/userRoutes");
const orderRoutes = require("./routes/orderRoutes");
const stockRoutes = require("./routes/stockRoutes");


/*mongodb connection & notification*/
mongoose.connect('mongodb+srv://jlumactud:jellyanne@batch139.poyy4.mongodb.net/Capstone2?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true});
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to Database`));


/*Middlewares*/
app.use(express.json())
app.use(express.urlencoded({extended:true}));
app.use(cors())


/*Routes*/

app.use("/api/users", userRoutes);
app.use("/api/orders", orderRoutes);
app.use("/api/stocks", stockRoutes);

app.listen(PORT, () => console.log(`Server running at port ${PORT}`))