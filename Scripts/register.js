console.log ("registration form")

let registerForm = document.getElementById("registerUser")

registerForm.addEventListener("submit", (event) => {
	event.preventDefault();
	console.log ("a")

	let firstName = document.getElementById('firstName').value
	let lastName = document.getElementById('lastName').value
	let address = document.getElementById('address').value
	let mobileNo = document.getElementById('mobileNo').value
	let email = document.getElementById('email').value
	let password = document.getElementById('pw').value
	let cpw = document.getElementById('cpw').value

	
	if (password == cpw && mobileNo.length >= 11) {

		console.log ("b")


		fetch("http://localhost:4000/api/users/email-exists", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					"email": email
				})
			}
		)
		.then (response => response.json())
		.then(data => {
			console.log(data)	

			if(data){

				fetch("http://localhost:4000/api/users/register",
					{
						method: "POST",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							address: address,
							email: email,
							mobileNo: mobileNo,
							password: password
						})
					}
				)
				.then(res => res.json())
				.then(data => {
				console.log(data)	

					if(data){
						alert(`Successfully Registered! You may now login.`)

						window.location.replace("./Pages/userLogin.html")

					} else {
						alert(`Something went wrong. Please try again!`)
					}
				})
			}
		})


	}
			
})