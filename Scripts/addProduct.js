console.log ("add products");

let addProduct = document.getElementById("addProduct")

// console.log ("1")

addProduct.addEventListener("submit", (event) => {
	event.preventDefault();
	// console.log ("a")

	let stockItem = document.getElementById('stockItem').value
	let stockDescription = document.getElementById('stockDescription').value
	let stockQty = document.getElementById('stockQty').value
	let stockPrice = document.getElementById('stockPrice').value
	let sellingPrice = document.getElementById('sellingPrice').value

			// console.log ("b")

			fetch("http://localhost:4000/api/stocks/addStock", {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},

				body: JSON.stringify ({
					stockItem:stockItem,
					stockDescription:stockDescription,
					stockQty:stockQty,
					stockPrice:stockPrice,
					sellingPrice:sellingPrice
				})
			})
			.then ( response => response.json())
			.then(data => {
				console.log(data)	

				if(data){
					alert(`Items Added`)

				

				} else {
					alert(`Something went wrong. Please try again!`)
				}
			})
		

})