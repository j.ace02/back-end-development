console.log ("login")

const login = document.getElementById ("loginUser");

login.addEventListener("submit", (e) => {
	e.preventDefault()

const email = document.getElementById("email").value
const pw = document.getElementById("pw").value


	if(email != "" || pw != ""){
		fetch("http://localhost:4000/api/users/login", 
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify({
					email: email,
					password: pw
				})
			}
		).then( response => response.json())
		.then(response => {
			console.log(response)	//object ---> access token

			// localStorage
				//setItem("property name", value)
				//getItem("property name")

				localStorage.setItem("token", response.access)

				if(response.access){

					let token = localStorage.getItem("token")

					fetch("http://localhost:4000/api/users/details", 
						{
							method: "GET",
							headers: {
								"Content-Type": "application",
								"Authorization": `Bearer ${token}`
							}
						}
					).then(response => response.json())
					.then(response => {
						console.log(response)	//user document
						const {isAdmin, _id} = response

						localStorage.setItem("isAdmin", isAdmin)
						localStorage.setItem("userId", _id)


						window.location.replace("./Pages/userLogin.html")
					})

				}
		})
		
	} else {
		alert("Please fill in email and password")
	}
})