const Stocks = require("./../models/Stocks");

const bcrypt = require("bcrypt");
const auth = require("./../auth");


module.exports.addStock = (reqBody) => {

	let newStocks = new Stocks ({

		stockItem : reqBody.stockItem,
		stockDescription : reqBody.stockDescription,
		stockQty : reqBody.stockQty,
		stockPrice : reqBody.stockPrice,
		sellingPrice : reqBody.sellingPrice

	})

	return newStocks.save().then( (result, error) => {

		if(result){
			return "New stock has been added to the system", result
		} else {
			return error
		}

	})

}

module.exports.getStocks = () => {

	return Stocks.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

module.exports.replenish = (id, reqBody) => {

	const {stockItem,stockDescription,stockQty, stockPrice, sellingPrice} = reqBody;

	let updateStocks = 
	{
		stockItem: reqBody.stockItem,
		stockDescription : reqBody.stockDescription,
		stockQty:reqBody.stockQty,
		stockPrice : reqBody.stockPrice,
		sellingPrice : reqBody.sellingPrice
	}

	return Stocks.findByIdAndUpdate(id, updateStocks, {new : true}).then( (result, error) => {

		if(error){
			return error
		} else {
			return "Stock has been replenish",result
		}

	})

}

module.exports.deleteStocks = (id, reqBody) => {

	const {

			stockItem,
			stockDescription,
			stockQty, 
			stockPrice, 
			sellingPrice

		  } = reqBody;

	let deleteStock = 
	{
		stockItem: reqBody.stockItem,
		stockDescription : reqBody.stockDescription,
		stockQty:reqBody.stockQty,
		stockPrice : reqBody.stockPrice,
		sellingPrice : reqBody.sellingPrice
	}

	return Stocks.findByIdAndDelete(id, deleteStock).then( (result, error) => {

		if(result == null){
			return `Item not existing`
		} else {
			if(result){
				
				return "Item has been deleted"
			} else {
				return error
			}
		}
	})
}

module.exports.getActiveStocks = (req,res) => {

	

	return Stocks.find({isActive: true}).then((result, err) => {
		if(err) {
			return false
		} else {
			return result
		}
	})
}


module.exports.archiveStocks = (reqBody) => {

	let itemStatus = {
		isActive: false
	}

	return Stocks.findOneAndUpdate({stockItem: reqBody}, itemStatus).then( (result, error) => {

		if(result == null){
			return `Item not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		}
	})

}

module.exports.unarchiveStocks = (reqBody) => {

	let itemStatus = {
		isActive: true
	}

	return Stocks.findOneAndUpdate({stockItem: reqBody}, itemStatus).then( (result, error) => {

		if(result == null){
			return `Item not existing`
		} else {
			if (result) {
				return true
			} else {
				return false
			}
		}
	})
}