const Orders = require("./../models/Orders");

module.exports.createOrder = (reqBody) => {

	let newOrders = new Orders ({

		orderName: reqBody.orderName,
		orderQty: reqBody.orderQty
		
	})

	return newOrders.save().then( (result, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})

}


module.exports.allActiveOrders = (reqBody) => {

	return Orders.find().then ( (result,error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}

module.exports.singleOrder = (params) => {

	return Orders.findById(params).then((result, err) => {
		// console.log(result)

		if(result == null){
			return `Order not existing`
		} else {
			if(result){
				return result
			} else {
				error
			}
		}
	})
}

module.exports.deleteOrders = (item) => {

	const {orderItem} = item;

	let deleteOrders = {orderItem:orderItem}

	return Orders.findOneAndDelete(deleteOrders).then( (result, error) => {

		if(result == null){
			return `Item not existing`
		} else {
			if(result){
				
				return "Item has been deleted"
			} else {
				return error
			}
		}
	})
}

module.exports.editOrders = (id, reqBody) => {

	const {orderItem, orderQty} = reqBody

	let editOrders = {

		orderItem: reqBody.orderItem,
		orderQty: reqBody.orderQty

	}

	return Orders.findByIdAndUpdate(id, editOrders, {new: true}).then( (result, error) => {

			if(error){
				return error
			} else {
				return result
			}
	})

}