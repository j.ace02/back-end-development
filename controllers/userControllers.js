const User = require("./../models/User");


const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports.checkEmail = (reqBody) => {
	const {email} = reqBody 

	return User.findOne({email: email}).then( (result, error) => {
		// console.log(email)
		if(result != null){
			return `Email already exists`
		} else {
			console.log(result)	//null bec email does not exist
			if(result == null){
				return "User has been registered"
			} else {
				return error 
			}
		}
	})
}


module.exports.register = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		address: reqBody.address,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	//save()
	return newUser.save().then( (result, error) => {
		if(result){
			return true
		} else {
			return error
		}
	})
}

module.exports.setAsAdmin = (id, reqBody) => {

	const {isAdmin} = reqBody;

	let updateUser = {isAdmin: isAdmin}

	return User.findByIdAndUpdate (id, updateUser, {new : true}).then( (result, error) => {

			if(error){
				return false
			} else {
				return result
			}
	})

}

module.exports.getAllUsers = () => {

	return User.find().then( (result, error) => {
		if(result){
			return result
		} else {
			return error
		}
	})
}


module.exports.login = (reqBody) => {
	const {email, password} = reqBody;

	return User.findOne({email: email}).then( (result, error) => {

		if(result == null){
			return false
		} else {
			//what if we found the email and is existing, but the pw is incorrect
			let isPasswordCorrect = bcrypt.compareSync(password, result.password)

			if(isPasswordCorrect == true){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	// console.log(data)
	const {id} = data

	return User.findById(id).then((result, err) => {
		// console.log(result)

		if(result != null){
			result.password = "";
			return result
		} else {
			return false
		}
	})
}


module.exports.inactiveAdmin = (req,res) => {

	let adminStatus = {

		isActive: false,
		email: req.email
	}

	return User.findOneAndUpdate({isActive: req}, adminStatus).then( (result, error) => {

		if(result == null){
			return `Admin not existing`
		} else {
			if (result) {
				return "Admin has been inactivated"
			} else {
				return false
			}
		}
	})
}

module.exports.activeAdmin = (req,res) => {

	let adminStatus = {
		isActive: true,
		email: req.email

	}

	return User.findOneAndUpdate({isActive: reqBody}, adminStatus).then( (result, error) => {

		if(result == null){
			return `Admin not existing`
		} else {
			if (result) {
				return "Admin has been activated"
			} else {
				return false
			}
		}
	})
}